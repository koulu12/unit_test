const expect = require('chai').expect;
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Our first unit tests', () => {
    before(() => {
        //initialization
        // create objects...
   
    });

    it('Can add 8 and 2 together', () => {
        // Tests
        expect(mylib.add(8,2)).equal(10, "8 + 2 is not 10, for some reason?");
    });
    it('Can divide 8 by 2', () => {
        // Tests
        expect(mylib.divide(8,2)).equal(4, "8 / 2 is not 4 for some reason");
    });
    
    it('Cannot divide by 0 in tests', () => {
        // Tests
        assert.throws(() => {
            mylib.divide(8,0);
        }, Error);
    });
 
    it('Can subtract 2 from 8', () => {
        // Tests
        expect(mylib.subtract(8,2)).equal(6, "8 - 2 is not 6 for some reason");
    });
    it('Can multiply 8 by 2', () => {
        // Tests
        expect(mylib.multiply(8,2)).equal(16, "8 * 2 is not 16 for some reason");
    });



    after(() => {
        // Cleanup
        // Shutdown the Express server
        console.log("Testing completed.");
    });
});