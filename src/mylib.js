
/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function */
    add: (a,b) => {
        const sum = a + b;
        return sum
    },
    subtract: (a,b) => {
        return a - b;
    },
    /** Singleline arrow function */
    //divide: (dividend,divisor) => dividend / divisor,
    divide: (a,b) => {
        if (b==0) {
            throw new Error("Cannot divide by 0");
        }
        return a/b;
    },

    /** Regular function in js */
    multiply: function(a,b) {
        return a * b;
    }
};

module.exports = mylib;